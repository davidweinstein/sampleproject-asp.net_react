﻿using Core.Interfaces.Managers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers
{
    [Route("api/todos")]
    public class ToDoController : Controller
    {
        private readonly IToDoManager _toDoManager;
        public ToDoController(IToDoManager toDoManager)
        {
            _toDoManager = toDoManager;
        }

        [HttpGet]
        public IActionResult GetTodos()
        {
            return Json(_toDoManager.GetToDos());
        }
    }
}
