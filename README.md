# Sample Project

---
## Structure

### Core
Models and interfaces to be used throughout the solution can be included here

### DataAccessHandlers
Anything dealing with Database access or data access from any external resource should go here, with an accompanying interface in the core project.

### Engines
Resuable chunks of business logic should go here, with an accompanying interface in the core project.
> Loading, Saving, Updating are usually good cases

### Managers
Larger workflow management, should make heavy use of the engine functionality you have available.  
Add an accompanying interface in the core project.
> These should probably be 1-to-1 with each Controller method in the web project

### Web
Presentation layer - this contains all of your React code and your API controllers
> Goal is to keep this mostly focused on the client-side of the application, and to keep the controllers as minimal as possible.

---
## Other Notes

### Dependency Injection
DI is mananged at each level. Look at the `Infrastructure/Configuration.cs` file that exists in each project that will be using DI. This is done to better separate the layers.

### Static
Don't use static methods with DI. It doesn't work. In .NET, it's better to make it an instance class or method, until it needs to be a static one, rather than the other way around.

---


## Running the Project
- Open the solution file.   
- Build  
- In the `Web/ClientApp` folder, `npm install`  
- Start IIS Express. (This will automatically run `npm start` for you)  

*Optional (if sql server instance is available)*
- *In Web/appsettings.json update the 'DatabaseConnectionString' value to point to your DB*
- *Run "Update-Database"*


