﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces.Handlers
{
	public interface IToDoHandler : IBaseHandler<ToDoEntity>
	{
	}
}
