﻿namespace Core.Models
{
	public interface IBaseEntity
	{
		int ID { get; set; }
	}
}
