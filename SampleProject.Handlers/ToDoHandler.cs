﻿using Core.Interfaces.Handlers;
using Core.Models;
using Handlers.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Handlers
{

	/*
		Handlers are used for data access (typically from a database)
		- No business logic should be performed inside the handlers
	*/
    public class ToDoHandler : BaseHandler<ToDoEntity>, IToDoHandler
    {
        public ToDoHandler(SampleProjectContext context) : base(context)
        {
        }

        // Any additional custom methods can go here
    }
}
