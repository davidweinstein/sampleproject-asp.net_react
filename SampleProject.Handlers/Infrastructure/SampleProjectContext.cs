﻿using Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Handlers.Infrastructure
{
	public class SampleProjectContext : DbContext
	{
		public SampleProjectContext(DbContextOptions options) : base(options)
		{
		}

		public virtual DbSet<ToDoEntity> ToDos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ToDoEntity>()
                .HasKey(t => t.ID);
        }
	}
}
