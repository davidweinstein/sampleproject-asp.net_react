﻿using Core.Interfaces.Handlers;
using Handlers;
using Handlers.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class Configuration
    {
        public static IServiceCollection AddDataAccessHandlers(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<SampleProjectContext>(); 
            services.AddDbContext<SampleProjectContext>(options => options.UseSqlServer(connectionString));

            services.AddTransient<IToDoHandler, ToDoHandler>();

            // TODO: Configure Entity Framework
            return services;
        } 
    }
}
