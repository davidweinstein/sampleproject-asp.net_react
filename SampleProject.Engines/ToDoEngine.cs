﻿using Core.Interfaces.Engines;
using Core.Interfaces.Handlers;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Engines
{
	/*
		Engines are used to hold shared business logic that multiple managers will want to access
		- Engines do not need to be one-to-one with managers or handlers
		- Engines should not call Managers or other engines (think about engine functions as small, self-contained units of logic)
		- Engines can call into handlers
	*/
    public class ToDoEngine : IToDoEngine
    {
        private readonly IToDoHandler _toDoHandler;
        public ToDoEngine(IToDoHandler toDoHandler)
        {
            _toDoHandler = toDoHandler;
        }

        public ToDoEntity CreateToDoEntity(string title, string description, DateTime? dueDate = null)
        {
            var todo = new ToDoEntity
            {
                Title = title,
                Description = description,
                DueDate = dueDate
            };
            _toDoHandler.Insert(todo);
            _toDoHandler.SaveChanges();
            return todo;
        }

        public ToDoEntity GetToDo(int id)
        {
            var todo = _toDoHandler.GetById(id);
            return todo;
        }

        public IEnumerable<ToDoEntity> GetToDos()
        {
            return _toDoHandler.GetAll();
        }

        public ToDoEntity UpdateDueDate(ToDoEntity entity, DateTime? dueDate = null)
        {
            entity.DueDate = dueDate;
            _toDoHandler.Update(entity);
            _toDoHandler.SaveChanges();
            return entity;
        }
    }
}
