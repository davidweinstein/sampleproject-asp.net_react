﻿using Core.Interfaces.Engines;
using Engines;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
	/*
		The configuration file is used to register our classes for dependency injection	 
	*/
    public static class Configuration
    {
        public static IServiceCollection AddEngines(this IServiceCollection services)
        {
            services.AddTransient<IToDoEngine, ToDoEngine>();

            return services;
        }
    }
}
