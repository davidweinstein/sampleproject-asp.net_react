﻿using Core.Interfaces.Managers;
using Managers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
	/*
		The configuration file is used to register our classes for dependency injection	 
	*/
    public static class Configuration
    {
        public static IServiceCollection AddManagers(this IServiceCollection services)
        {
            services.AddTransient<IToDoManager, ToDoManager>();

            return services;
        }
    }
}
