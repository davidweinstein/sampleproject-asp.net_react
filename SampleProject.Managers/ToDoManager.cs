﻿using Core.Interfaces.Engines;
using Core.Interfaces.Managers;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;


/*
	Managers are a place to hold business logic
	- Each manager function should be considered a single and complete use case
	- Public manager functions should not call into other public manager functions
		- Logic shared between functions in the same manager class should be placed in a private function
		- Logic shared between functions in separate managers should be placed in an engine

		- NOTE: In this example we are calling into the TodoEngine for each method. This is for demonstration,
		- but is not actually following our standards. You should only be calling into an engine if the
		- logic is shared between managers. You are not required to call into an engine for every manager function.

	- NOTE: Not shown here, but it is perfectly acceptable to call a handler directly from a manager
*/
namespace Managers
{
    public class ToDoManager : IToDoManager
    {
        private readonly IToDoEngine _toDoEngine;
        public ToDoManager(IToDoEngine toDoEngine)
        {
            _toDoEngine = toDoEngine;
        }

        public ToDoEntity AddNewToDo(string title, string description, DateTime? dueDate = null)
        {
            return _toDoEngine.CreateToDoEntity(title, description, dueDate);
        }

        public IEnumerable<ToDoEntity> GetToDos()
        {
            return _toDoEngine.GetToDos();
        }

        public ToDoEntity UpdateDueDate(int id, DateTime? dueDate = null)
        {
            var entity = _toDoEngine.GetToDo(id);
            return _toDoEngine.UpdateDueDate(entity, dueDate);
        }
    }
}
